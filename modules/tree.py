# APPADOO APOORVA SRINIVAS | BURDESE YAEL
from turtle import Turtle

from modules.drawing import bezier_2, rgb2hex


class Tree:
    """ Class that stores the scale information of a Tree and then draws it when called"""

    def __init__(self, scale=(1, 1), start_pos=(0, 0)):
        self.scale = scale
        self.scale_mean = (scale[0] + scale[1]) / 2
        self.tu = Turtle(undobuffersize=0)
        self.tu.penup()
        self.tu.ht()
        self.pos = start_pos
        self.original_pos = start_pos
        self.original_scale = scale
        self.counter = 0  # used for gradient

    def apply_scale(self, x, y):
        return (x + self.pos[0]) * self.scale[0], (y + self.pos[1]) * self.scale[1]

    def apply_scale_bezier_2(self, x1, y1, x2, y2, x3, y3):
        x1 = (x1 + self.pos[0]) * self.scale[0]
        y1 = (y1 + self.pos[1]) * self.scale[1]
        x2 = (x2 + self.pos[0]) * self.scale[0]
        y2 = (y2 + self.pos[1]) * self.scale[1]
        x3 = (x3 + self.pos[0]) * self.scale[0]
        y3 = (y3 + self.pos[1]) * self.scale[1]
        return x1, y1, x2, y2, x3, y3,

    def draw(self, amount=10, tu=None):
        if not tu:
            tu = self.tu
        # trunk
        rect_tu = Turtle()
        rect_tu.up()
        rect_tu.ht()
        rect_tu.goto(self.apply_scale(0, 0))
        rect_tu.pensize(10 * self.scale_mean)
        rect_tu.fillcolor('#582900')
        rect_tu.goto(self.apply_scale(-25, 100))
        rect_tu.down()
        rect_tu.begin_fill()
        rect_tu.goto(self.apply_scale(-25, 100))
        rect_tu.goto(self.apply_scale(25, 100))
        rect_tu.goto(self.apply_scale(25, -25))
        rect_tu.goto(self.apply_scale(-25, -25))
        rect_tu.goto(self.apply_scale(-25, 100))
        rect_tu.end_fill()

        # tree

        # defining decoration positions
        x1g1 = y1g1 = x3g1 = y3g1 = x2g1 = y2g1 = x1g2 = y1g2 = x3g2 = y3g2 = x2g2 = y2g2 = \
            x1g3 = y1g3 = x3g3 = y3g3 = x2g3 = y2g3 = xb1 = yb1 = xb2 = yb2 = xb3 = yb3 = None
        tree_level = None
        # init turtle object
        tu.color('black')
        tu.fillcolor('green')

        # drawing leaves
        for tree_level in range(1, amount + 1):
            self.counter += 1
            tu.fillcolor(rgb2hex((0, (int(100 + self.counter * 255 / (amount * 5))), 0)))
            tree_scale = (0.9 ** tree_level, 0)
            self.pos = (self.pos[0] * 1.1111111111111111111, self.pos[1] + 500 / amount)
            tu.goto(self.tu.pos())
            tu.begin_fill()
            # tracing bezier2
            x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(0, 0, -49.0, -66.0, -94.0, -2.0)
            x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
            bezier_2(x1, y1, x2, y2, x3, y3, tu)

            # storing 3rd coordinate for the red garland
            if tree_level == 2:
                x3g1 = x2
                y3g1 = y2

            # storing coordinate for the red ball
            if tree_level == int(3 * amount / 5):
                xb1 = x2
                yb1 = y2

            # tracing bezier2
            x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(-94.0, -2.0, -155.0, -48.0, -216.0, -2.0)
            x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
            bezier_2(x1, y1, x2, y2, x3, y3, tu)

            # storing 2nd coordinate for the blue garland
            if tree_level == 2:
                x2g2 = x1
                y2g2 = y1

            # storing 2nd coordinate for the yellow garland
            if tree_level == int(6 * amount / 10):
                x2g3 = x1
                y2g3 = y1

            # tracing bezier2
            x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(-216.0, -2.0, -108.0, 4.0, -80.0, 64.0)
            x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
            bezier_2(x1, y1, x2, y2, x3, y3, tu)

            # storing 1st coordinate for the red garland
            if tree_level == amount - 1:
                x1g1 = x3
                y1g1 = y3

            # mirrored other half
            # tracing bezier2
            x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(80.0, 64.0, 108.0, 4.0, 216.0, -2.0)
            x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
            bezier_2(x1, y1, x2, y2, x3, y3, tu)

            # tracing bezier2
            x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(216.0, -2.0, 155.0, -48.0, 94.0, -2.0)
            x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
            bezier_2(x1, y1, x2, y2, x3, y3, tu)

            # storing 2nd coordinate for the red garland
            if tree_level == 1:
                x2g1 = x3
                y2g1 = y3

            # storing 1st coordinate for the blue garland
            if tree_level == int(amount / 2):
                x1g2 = x3
                y1g2 = y3

            # storing 1st coordinate for the yellow garland
            if tree_level == int(9 * amount / 10):
                x1g3 = x3
                y1g3 = y3

            # tracing bezier2
            x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(94.0, -2.0, 49.0, -66.0, 0, 0)
            x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
            bezier_2(x1, y1, x2, y2, x3, y3, tu)

            # storing 3rd coordinate for the blue garland
            if tree_level == 3:
                x3g2 = x2
                y3g2 = y2

            # storing coordinate for the blue ball
            if tree_level == amount:
                xb2 = x2
                yb2 = y2

            # storing coordinate for the yellow ball
            if tree_level == int(2 * amount / 10):
                xb3 = x2
                yb3 = y2

            # storing 3rd coordinate for the yellow garland
            if tree_level == int(8 * amount / 10):
                x3g3 = x2
                y3g3 = y2

            tu.end_fill()

        # Drawing top
        tree_scale = (0.9 ** tree_level, 0)
        tu.begin_fill()

        # tracing bezier2
        x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(0, 0, -49.0, -66.0, -94.0, -2.0)
        x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
        bezier_2(x1, y1, x2, y2, x3, y3, tu)
        # tracing bezier2

        x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(-94.0, -2.0, -155.0, -48.0, -216.0, -2.0)
        x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
        bezier_2(x1, y1, x2, y2, x3, y3, tu)
        # tracing bezier2

        x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(-216.0, -2.0, -108.0, 4.0, 0, 150)
        x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
        bezier_2(x1, y1, x2, y2, x3, y3, tu)

        # tracing bezier2
        x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(0, 0, 49.0, -66.0, 94.0, -2.0)
        x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
        bezier_2(x1, y1, x2, y2, x3, y3, tu)
        # tracing bezier2

        x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(94.0, -2.0, 155.0, -48.0, 216.0, -2.0)
        x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
        bezier_2(x1, y1, x2, y2, x3, y3, tu)
        # tracing bezier2

        x1, y1, x2, y2, x3, y3 = self.apply_scale_bezier_2(216.0, -2.0, 108.0, 4.0, 0, 150)
        x1, x2, x3 = x1 * tree_scale[0], x2 * tree_scale[0], x3 * tree_scale[0]
        bezier_2(x1, y1, x2, y2, x3, y3, tu)

        tu.end_fill()

        # star
        self.scale = self.original_scale
        star = Turtle()
        # star.pensize(5 * self.scale_mean)
        star.ht()
        star.color('yellow')
        star.fillcolor('yellow')
        star.up()
        star.goto(tu.pos())
        star.down()
        star.begin_fill()
        star.backward(45 * self.scale_mean)
        for i in range(5):
            star.forward(90 * self.scale_mean)
            star.right(144)
        star.end_fill()

        # garlands
        g1 = Turtle()
        g1.ht()
        g1.up()
        g1.pensize(10 * self.scale_mean)
        g1.color('red')
        bezier_2(x1g1, y1g1, x3g1, y3g1, x2g1, y2g1, g1)

        g2 = Turtle()
        g2.ht()
        g2.up()
        g2.pensize(10 * self.scale_mean)
        g2.color('blue')
        bezier_2(x1g2, y1g2, x3g2, y3g2, x2g2, y2g2, g2)

        g3 = Turtle()
        g3.ht()
        g3.up()
        g3.pensize(10 * self.scale_mean)
        g3.color('yellow')
        bezier_2(x1g3, y1g3, x3g3, y3g3, x2g3, y2g3, g3)

        # balls
        b1 = Turtle()
        b1.ht()
        b1.up()
        b1.pensize(2 * self.scale_mean)
        b1.color('black')
        b1.fillcolor('red')
        b1.goto(xb1, yb1)
        b1.down()
        b1.begin_fill()
        b1.circle(15 * self.scale_mean)
        b1.end_fill()

        b2 = Turtle()
        b2.ht()
        b2.up()
        b2.pensize(2 * self.scale_mean)
        b2.color('black')
        b2.fillcolor('blue')
        b2.goto(xb2, yb2)
        b2.down()
        b2.begin_fill()
        b2.circle(15 * self.scale_mean)
        b2.end_fill()

        b3 = Turtle()
        b3.ht()
        b3.up()
        b3.pensize(2 * self.scale_mean)
        b3.color('black')
        b3.fillcolor('yellow')
        b3.goto(xb3, yb3)
        b3.down()
        b3.begin_fill()
        b3.circle(15 * self.scale_mean)
        b3.end_fill()

        self.pos = self.original_pos
        self.counter = 0


if __name__ == '__main__':
    from turtle import Screen, mainloop, update

    screen = Screen()
    screen.tracer(0)
    print('is main')

    _tu = Turtle()
    _tu.penup()
    tree = Tree(scale=(1.5 * screen.window_width() / 1366, 0.8 * screen.window_height() / 768), start_pos=(-100, -100))
    tree.draw(10)
    # tree.draw(20)
    update()

    mainloop()
