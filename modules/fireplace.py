# APPADOO APOORVA SRINIVAS | BURDESE YAEL
from turtle import Turtle


class Fireplace:
    """ Class that stores the scale information of a fireplace and then draws it when called"""

    def __init__(self, scale=(1, 1), start_pos=(0, 0)):
        self.scale = scale
        self.scale_mean = (scale[0] + scale[1]) / 2
        self.tu = Turtle(undobuffersize=0)
        self.tu.penup()
        self.tu.ht()
        self.pos = start_pos
        self.original_pos = start_pos
        self.original_scale = scale
        self.counter = 0  # used for gradient

    def apply_scale(self, x, y):
        return (x + self.pos[0]) * self.scale[0], (y + self.pos[1]) * self.scale[1]

    def apply_scale_bezier_2(self, x1, y1, x2, y2, x3, y3):
        x1 = (x1 + self.pos[0]) * self.scale[0]
        y1 = (y1 + self.pos[1]) * self.scale[1]
        x2 = (x2 + self.pos[0]) * self.scale[0]
        y2 = (y2 + self.pos[1]) * self.scale[1]
        x3 = (x3 + self.pos[0]) * self.scale[0]
        y3 = (y3 + self.pos[1]) * self.scale[1]
        return x1, y1, x2, y2, x3, y3,

    def draw(self):
        # fireplace
        base = Turtle()
        base.up()
        base.ht()
        base.pensize(10 * self.scale_mean)
        base.color('grey')
        base.fillcolor('#6e3000')

        # bottom of the fireplace
        base.goto(self.apply_scale(-150, 230))
        base.down()
        base.begin_fill()
        base.goto(self.apply_scale(150, 230))
        base.goto(self.apply_scale(150, 0))
        base.goto(self.apply_scale(-150, 0))
        base.goto(self.apply_scale(-150, 230))
        base.end_fill()

        # separation between the two parts
        base.up()
        base.goto(self.apply_scale(-170, 235))
        base.down()
        base.goto(self.apply_scale(170, 235))
        base.up()
        base.goto(self.apply_scale(-180, 245))
        base.down()
        base.goto(self.apply_scale(180, 245))
        base.up()
        base.goto(self.apply_scale(-190, 255))
        base.down()
        base.goto(self.apply_scale(190, 255))

        # top of the fireplace
        base.up()
        base.goto(self.apply_scale(-150, 700))
        base.down()
        base.begin_fill()
        base.goto(self.apply_scale(150, 700))
        base.goto(self.apply_scale(150, 250))
        base.goto(self.apply_scale(-150, 250))
        base.goto(self.apply_scale(-150, 700))
        base.end_fill()

        # fire
        fire = Turtle()
        fire.up()
        fire.ht()
        fire.goto(self.apply_scale(0, 0))
        fire.pensize(10 * self.scale_mean)
        fire.color('grey')
        fire.fillcolor('black')
        fire.goto(self.apply_scale(-75, 150))
        fire.down()
        fire.begin_fill()
        fire.goto(self.apply_scale(75, 150))
        fire.goto(self.apply_scale(75, 0))
        fire.goto(self.apply_scale(-75, 0))
        fire.goto(self.apply_scale(-75, 150))
        fire.end_fill()


if __name__ == '__main__':
    from turtle import Screen, mainloop, update

    screen = Screen()
    screen.tracer(5)
    print('is main')
    screen.setup(width=1.0, height=1.0)
    _tu = Turtle()
    _tu.penup()
    fireplace = Fireplace(scale=(1 * screen.window_width() / 1366, 0.8 * screen.window_height() / 768),
                          start_pos=(0, 0))
    fireplace.draw()
    update()

    mainloop()
