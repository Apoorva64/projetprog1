# APPADOO APOORVA SRINIVAS | BURDESE YAEL
from turtle import Turtle


class Plate_rectangle:
    """ Plate stores the information about the scale of the Plate and draws it with draw"""

    def __init__(self, scale=(1, 1), start_pos=(0, 0), ):
        self.scale = scale
        self.pos = start_pos
        # init tu
        self.tu = Turtle()
        self.tu.ht()
        self.tu.penup()

    def apply_scale(self, x, y):
        """ apply scale to a point"""
        return (x + self.pos[0]) * self.scale[0], (y + self.pos[1]) * self.scale[1]

    def draw(self, tu=None):
        if not tu:
            tu = self.tu
        tu.pensize(2* (self.scale[0]+self.scale[1])/2)
        tu.color('#666666', '#dddddd')
        # outer borders
        tu.begin_fill()
        tu.goto(self.apply_scale(-136.0, -200.0))
        tu.down()
        tu.goto(self.apply_scale(-123.0, -151.0))
        tu.goto(self.apply_scale(123.0, -151.0))
        tu.goto(self.apply_scale(136.0, -200.0))
        tu.goto(self.apply_scale(-136.0, -200.0))
        tu.end_fill()

        # inner borders
        tu.begin_fill()
        tu.goto(self.apply_scale(-125.0, -199.0))
        tu.down()
        tu.goto(self.apply_scale(-113.0, -156.0))
        tu.goto(self.apply_scale(113.0, -156.0))
        tu.goto(self.apply_scale(125.0, -199.0))
        tu.goto(self.apply_scale(-125.0, -199.0))
        tu.end_fill()

        # center borders
        tu.begin_fill()
        tu.goto(self.apply_scale(-117.0, -199.0))
        tu.down()
        tu.goto(self.apply_scale(-107.0, -160.0))
        tu.goto(self.apply_scale(107.0, -160.0))
        tu.goto(self.apply_scale(117.0, -199.0))
        tu.goto(self.apply_scale(-117.0, -199.0))
        tu.up()
        tu.end_fill()

if __name__ == '__main__':
    from turtle import Screen, mainloop, update

    screen = Screen()

    print('is main')
    _tu = Turtle()
    _tu.penup()
    tree = Plate_rectangle(scale=(2 * screen.window_width() / 1366, 2 * screen.window_height() / 768), start_pos=(0, 10))
    tree.draw()
    update()

    mainloop()