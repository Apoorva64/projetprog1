# APPADOO APOORVA SRINIVAS | BURDESE YAEL
from turtle import Turtle, Shape

from modules.drawing import bezier_2

scale = [1, 1]
pos = (0, 0)
WriteStep = 60


def apply_scale_bezier_2(x1, y1, x2, y2, x3, y3, tu):
    """applies scale to bezier 2 coordinates"""
    x1 = (x1 + pos[0]) * scale[0]
    y1 = (y1 + pos[1]) * scale[1]
    x2 = (x2 + pos[0]) * scale[0]
    y2 = (y2 + pos[1]) * scale[1]
    x3 = (x3 + pos[0]) * scale[0]
    y3 = (y3 + pos[1]) * scale[1]
    return x1, y1, x2, y2, x3, y3, tu


def apply_scale_line(x1, y1, x2, y2, tu):
    """applies scale to line coordinates"""
    x1 = (x1 + pos[0]) * scale[0]
    y1 = (y1 + pos[1]) * scale[1]
    x2 = (x2 + pos[0]) * scale[0]
    y2 = (y2 + pos[1]) * scale[1]
    return x1, y1, x2, y2, tu


def get_fire_turtle(screen):
    """
    Adds the fire shape to a screen instance
    :param screen: screen instance to add the fire to the background"""
    global scale
    tu = Turtle(undobuffersize=0)
    tu.ht()
    tu.up()
    # 1st fire shape
    fire_shape = Shape("compound")
    tu.begin_poly()

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(0, 0, -40, 30, -50, 100, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-50, 100, -80, 70, -50, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-50, 0, -60, 90, -100, 110, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-100, 110, -80, 80, -90, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(0, 0, 40, 30, 50, 100, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(50, 100, 80, 70, 50, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(50, 0, 60, 90, 100, 110, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(100, 110, 80, 80, 90, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-20, 0, -15, 90, 0, 120, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(0, 120, 15, 90, 20, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    tu.end_poly()
    # adding fire to the shape
    fire_shape.addcomponent(tu.get_poly(), "orange", "red")
    tu.begin_poly()
    tu.up()

    x1, y1, x2, y2, tu = apply_scale_line(100, 0, -100, -50, tu)
    tu.goto(x1, y1)
    tu.down()
    tu.goto(x1, y2)
    tu.goto(x2, y2)
    tu.goto(x2, y1)

    tu.end_poly()
    # adding wood to the shape
    fire_shape.addcomponent(tu.get_poly(), "#582900", "#582900")
    screen.addshape('fire_shape', shape=fire_shape)

    # 2nd fire shape
    tu.up()
    fire_shape2 = Shape("compound")
    tu.begin_poly()

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(100, 0, 60, 30, 50, 100, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(50, 100, 20, 70, 50, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(50, 0, 40, 90, 0, 110, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(0, 110, 20, 80, 10, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-100, 0, -60, 30, -50, 100, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-50, 100, -20, 70, -50, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-50, 0, -40, 90, 0, 110, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(0, 110, -20, 80, -10, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(-20, 0, -15, 90, 0, 120, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)
    x1, y1, x2, y2, x3, y3, tu = apply_scale_bezier_2(0, 120, 15, 90, 20, 0, tu)
    bezier_2(x1, y1, x2, y2, x3, y3, tu)

    tu.end_poly()
    # adding fire to the shape
    fire_shape2.addcomponent(tu.get_poly(), "orange", "red")
    tu.up()
    tu.begin_poly()

    x1, y1, x2, y2, tu = apply_scale_line(100, 0, -100, -50, tu)
    tu.goto(x1, y1)
    tu.down()
    tu.goto(x1, y2)
    tu.goto(x2, y2)
    tu.goto(x2, y1)

    tu.end_poly()
    # adding wood to the shape
    fire_shape2.addcomponent(tu.get_poly(), "#582900", "#582900", )
    screen.addshape('fire_shape2', shape=fire_shape2)
    tu.clear()
    tu.ht()
